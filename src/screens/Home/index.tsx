import * as S from './styles';
import BoasVindas from "../../components/BoasVindas";
import SkeletonBoasVindas from "../../components/SkeletonBoasVindas";
import { useEffect, useState } from "react";

export default function Home(){

  const [aguardando, setAguardando] = useState(true);
  
  useEffect(() => {

    setTimeout(() => {
        setAguardando(false);
    }, 3000);

  },[])

    return(
        <S.Container>
            {
                aguardando === true ? <SkeletonBoasVindas/> : <BoasVindas/>
            }
        </S.Container>
    )
}