import * as S from './styles';

export default function BoasVindas(){
    return(
        <S.Header>
            
            <S.Avatar 
                source={{ uri:"https://github.com/gabynogueira.png" }} 
            />
        
            <S.Container>
                <S.Saudações>Olá</S.Saudações>
                <S.NomeDeUsuário>Gabriela Nogueira</S.NomeDeUsuário>
            </S.Container>

        </S.Header>
    )
}