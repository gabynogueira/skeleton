import Home from './src/screens/Home';
import dark from './src/theme/dark';
import light from './src/theme/light';
import { StatusBar } from 'expo-status-bar';
import { ThemeProvider } from 'styled-components/native';

export default function App() {
  return (
    <>   
      <StatusBar 
        style="light" 
        backgroundColor='transparent' 
        translucent
      />
      <ThemeProvider theme={dark}>
        <StatusBar style='auto' />
        <Home/>
      </ThemeProvider>
    </>
  );
}