import styled from "styled-components/native";

export const Container = styled.View`

`;
    
export const Header = styled.SafeAreaView`
    margin-bottom: 32px;
    flex-direction: row;
    align-items: center;
    gap: 12px;
`;

export const Avatar = styled.Image`
    height: 64px;
    width: 64px;
    border-radius: 32px;
`;

export const Saudações = styled.Text`
    color: ${({theme}) => theme.COLORS.TEXT};
    font-size: ${({theme}) => theme.TAMANHOS.TEXT};
`;

export const NomeDeUsuário = styled.Text`
    color: ${({theme}) => theme.COLORS.TEXT};
    font-size: ${({theme}) => theme.TAMANHOS.TEXT_DESTACADO};
    font-weight: bold;
`;