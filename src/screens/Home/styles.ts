import styled from "styled-components/native";

export const Container = styled.SafeAreaView`
    flex: 1;
    background: ${({ theme }) => theme.COLORS.BACKGROUND}; 
    padding: 32px;
`;
