export default {
    COLORS: {
        BACKGROUND: '#FFF',
        TEXT: '#000000',
        
    },
    TAMANHOS: {
        TEXT: "14px",
        TEXT_DESTACADO: "16px"
    }
}
