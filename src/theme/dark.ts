export default {
    COLORS: {
        BACKGROUND: '#000',
        TEXT: '#FFFFFF',
        
    },
    TAMANHOS: {
        TEXT: "14px",
        TEXT_DESTACADO: "16px"
    }
}
